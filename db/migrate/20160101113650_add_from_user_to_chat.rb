class AddFromUserToChat < ActiveRecord::Migration[5.2]
  def change
    add_column :chats, :from_user_id, :integer, null: false, default: 0
  end
end
