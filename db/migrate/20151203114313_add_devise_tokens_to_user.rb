class AddDeviseTokensToUser < ActiveRecord::Migration[5.2]
  def change
    change_table(:users) do |t|
      t.string :provider, :null => false, :default => "email"
      t.string :uid, :null => false, :default => ""
      ## Tokens
      t.text :tokens, array: true
    end
  end
end
