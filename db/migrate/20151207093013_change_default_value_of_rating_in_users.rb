class ChangeDefaultValueOfRatingInUsers < ActiveRecord::Migration[5.2]
  def up
    change_column_default :users, :rating, 0
  end

  def down
    change_column_default :users, :rating, nil
  end
end
