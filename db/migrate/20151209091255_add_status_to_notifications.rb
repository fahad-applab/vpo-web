class AddStatusToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :status, :integer, index: true, default: 0, null: false
  end
end
