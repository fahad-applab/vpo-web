class AddIsReadToChats < ActiveRecord::Migration[5.2]
  def change
    add_column :chats, :is_read, :boolean, default: false
  end
end
