class AddJobIdToRating < ActiveRecord::Migration[5.2]
  def change
    add_column :ratings, :job_id, :integer, default: nil

    add_index :ratings, :job_id
  end
end
