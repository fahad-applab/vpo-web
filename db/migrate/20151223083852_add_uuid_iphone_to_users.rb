class AddUuidIphoneToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :uuid_iphone, :string
  end
end
