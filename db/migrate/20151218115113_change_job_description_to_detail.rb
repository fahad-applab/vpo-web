class ChangeJobDescriptionToDetail < ActiveRecord::Migration[5.2]
  def change
    rename_column :jobs, :description, :detail
  end
end
