class AddCommentToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :comment, :string
  end
end
