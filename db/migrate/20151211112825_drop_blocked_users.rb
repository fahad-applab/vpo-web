class DropBlockedUsers < ActiveRecord::Migration[5.2]
  def change
    drop_table :blocked_users
  end
end
