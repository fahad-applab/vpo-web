class AddIsAdminNotificationToRpushNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :rpush_notifications, :is_admin_notification, :boolean, default: false
    add_index :rpush_notifications, :is_admin_notification
  end
end
