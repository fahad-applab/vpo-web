class CreateAdminService
  def call
    eula = Eula.create(eula_text: '', is_latest: true)
    privacy = Privacy.create(privacy_text: '', is_latest: true)
    user = User.find_or_create_by!(email: Rails.application.credentials[Rails.env.to_sym][:admin_email]) do |user|
        user.email = 'admin1@fram.com'
        user.username = 'admin1'
        user.user_type = User.user_types[:administrator]
        user.password = Rails.application.credentials[Rails.env.to_sym][:admin_password]
        user.password_confirmation = Rails.application.credentials[Rails.env.to_sym][:admin_password]
        user.eula = eula
        user.privacy = privacy
        user.confirm
      end
  end
end
